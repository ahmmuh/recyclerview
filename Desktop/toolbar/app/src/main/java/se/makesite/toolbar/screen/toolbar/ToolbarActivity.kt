package se.makesite.toolbar.screen.toolbar

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class ToolbarActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_toolbar)
    }
}
