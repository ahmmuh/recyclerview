package se.makesite.data

import android.support.v7.widget.DialogTitle
import com.google.gson.annotations.SerializedName

class Products (

    @SerializedName("name")
    val title: String,
    @SerializedName("photo_url")
    val photosUrl : String,
    val price: String
)