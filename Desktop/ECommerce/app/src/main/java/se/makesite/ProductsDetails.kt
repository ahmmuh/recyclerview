package se.makesite

import android.content.DialogInterface
import android.os.Bundle
import android.support.compat.R.id.text
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.product_detalis.*
import se.makesite.ecommerce.R
import java.net.URI.create

class ProductsDetails : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_detalis)

        val title = intent.getStringExtra("title")
        val photoUrl = intent.getStringExtra("photoUrl")


        productName.text = title
        Picasso.get().load(photoUrl).into(photo)

        available.setOnClickListener {
            AlertDialog.Builder(this)
                .setMessage("hey, $title in stock, ")
                .setPositiveButton("ok") { dialog, which ->

                }
                .create()
                .show()

        }
    }
}