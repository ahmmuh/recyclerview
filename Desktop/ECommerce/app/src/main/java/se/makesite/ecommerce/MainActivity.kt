package se.makesite.ecommerce

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log.d
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.GridLayout

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.main.*
import se.makesite.JeansFragment
import se.makesite.MainFragment
import se.makesite.data.Products

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
         supportFragmentManager.beginTransaction()
             .replace(R.id.frameLayout, MainFragment())
             .commit()

        // make items cliclable
        nav_view.setNavigationItemSelectedListener {

            when(it.itemId){
                R.id.action_home -> {
                        supportFragmentManager.beginTransaction()
                            .replace(R.id.frameLayout, MainFragment())
                            .commit()

                    }

                R.id.action_socks -> d("socks", "was pressed")

                R.id.action_jeans -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, JeansFragment())
                        .commit()

                    d("Jeans","was sold")

                }
                R.id.action_jacks -> d("jacks","buy now")
            }
            it.isCheckable = true
            drawer.closeDrawers()
            true
        }


        // how to set an icon on the drawer home
      supportActionBar?.apply {
       setDisplayHomeAsUpEnabled(true)
          setHomeAsUpIndicator(R.drawable.ic_home_white_24dp)
      }


    }

    // open home icon to see a list of item in the drawer
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        drawer.openDrawer(GravityCompat.START)
        return true
    }
}
