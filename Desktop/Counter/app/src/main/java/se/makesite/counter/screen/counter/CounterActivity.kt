package se.makesite.counter.screen.counter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_counter.*
import se.makesite.counter.R

class CounterActivity : AppCompatActivity() {

    var counter = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_counter)

        reset_btn.setOnClickListener{
            counter = 0
            update()

        }
        counter_btn.setOnClickListener{
            counter++
            update()
        }
    }

    fun update(){
        counter_text_lable.text = counter.toString()
    }
}
