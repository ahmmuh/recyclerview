package se.makesite.counter.screen.counter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import se.makesite.counter.R

class TestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
    }
}
