package se.makesite.recycler3

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class UsersAdapter(val users: ArrayList<String>) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.user_row, parent,false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
    return users.size

    }



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.firstName.text =users[position]

    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        val firstName: TextView = itemView.findViewById(R.id.firstName)


    }

}